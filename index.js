require('dotenv').config();

const express = require('express');
const app = express();
const http = require('http').createServer(app);
const io = require('socket.io')(http);
const fetch = require('node-fetch')
const cookieSession = require('cookie-session');

// add CLIENT_ID and CLIENT_SECRET to .env file
const client_id = process.env.CLIENT_ID;
const client_secret = process.env.CLIENT_SECRET;
const cookie_secret = 'some_cookie_secret';

app.use(cookieSession({
        secret: cookie_secret,
        name: "webim_cookie"
    })
);

app.get('/', (req, res) => {
    res.redirect('/success')
});

app.get('/login/github', (req, res) => {  // login logic
    const redirect_uri = 'https://webim-tz.herokuapp.com/login/github/callback';
    res.redirect(
        `https://github.com/login/oauth/authorize?client_id=${client_id}&redirect_uri=${redirect_uri}`
    );
});

async function getAccessToken({code, client_id, client_secret}) {  // get access_token with oauth
    const request = await fetch('https://github.com/login/oauth/access_token', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            client_id,
            client_secret,
            code
        })
    });
    const text = await request.text();
    const params = new URLSearchParams(text);
    return params.get('access_token');
}

async function fetchGitHubUser(token) {  // return info in JSON about logged user
    const request = await fetch('https://api.github.com/user', {
        headers: {
            Authorization: 'token ' + token
        }
    });
    return await request.json();
}

app.get('/login/github/callback', async (req, res) => {
    const code = req.query.code;
    const access_token = await getAccessToken({code, client_id, client_secret});
    const user = await fetchGitHubUser(access_token);
    if (user) {  // if user has access_token then redirect to unauthorized page where will redirect to success
        req.session.access_token = access_token;
        res.redirect('/success');
    } else {
        res.send('Login did not succeed!');
    }
});


app.get('/unauthorized', async (req, res) => {
    if (req.session.access_token) {
        res.redirect('/success');  // redirect to success page if logged in
    } else {
        res.sendFile(__dirname + '/index.html');  // redirect to unauthorized (index) paged if not
    }
});

app.get('/success', async (req, res) => {
    if (req.session.access_token) {
        res.sendFile(__dirname + '/success.html');  // show success page if logged in
    } else {
        res.redirect('/unauthorized');  // show unauthorized page if not logged in
    }
})

app.get('/logout', (req, res) => {
    if (req.session) {
        req.session = null  // destroy session
    }
    res.redirect('/');  // redirect to root after destroy
});


let generatedNumberOnServer;  // global variable to store last generated number

const generateRandomNumber = () => {  // generate and return random number
    generatedNumberOnServer = Math.floor(Math.random() * 100);
    console.log('generated on server: ', generatedNumberOnServer);
    return generatedNumberOnServer
}

const sendIOToClient = number => {  // send to clients some number
    io.emit('random number', number);
}

const sendGeneratedNumberToClient = () => {  // send to clients generated random numbers
    sendIOToClient(
        generateRandomNumber()
    );
}

io.on('connection', (socket) => {
    console.log('user connected');
    sendIOToClient(generatedNumberOnServer);  // immediate send random number to connected user
    socket.on('disconnect', () => {
        console.log('user disconnected');
    })
});

sendGeneratedNumberToClient();  // first function - generate first random number before setInterval loop
setInterval(sendGeneratedNumberToClient, 5000); // second function - infinite loop with setInterval

http.listen(process.env.PORT || 3000, () => {
    console.log('Server started on *:3000');
});
